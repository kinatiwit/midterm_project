Local:
docker build -t 12262/flower-api .
docker build -t 12262/frontend .
docker push 12262/flower-api
docker push 12262/frontend

GCP:
docker pull 12262/flower-api
docker pull kinatiwit/frontend 
docker network create -d overlay frontend
docker network create -d overlay backend
docker service create --name redis-flowerapp --network backend redis 
docker service create --name frontend --network frontend -p 80:80 12262/frontend
docker service create --name flowers-api--replicas 3 -p 8000:8000 --mount type=volume,source=db-flower,target=/data --network frontend --network backend 12262/flowers-api gunicorn--bind 0.0.0.0:8000 main:app

Member:
Atiwit Tuekla 6022781007
Kittipong Suwannagarn 6022792871 